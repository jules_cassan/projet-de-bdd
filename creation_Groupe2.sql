#CASSAN Jules 21905789
#TRIMBUR Léo 22210320


CREATE TABLE personnel(
   id_pers INT AUTO_INCREMENT,
   nom_pers VARCHAR(50),
   prenom_pers VARCHAR(50),
   date_nais_pers DATE,
   adresse_pers VARCHAR(50),
   salaire VARCHAR(50),
   CONSTRAINT PK_PERSONNEL PRIMARY KEY(id_pers)
);

CREATE TABLE guide(
   id_guide INT,
   langue_guide VARCHAR(50) DEFAULT 'Francais',
   CONSTRAINT PK_GUIDE PRIMARY KEY(id_guide),
   CONSTRAINT FK_GUIDE_PERSONNEL FOREIGN KEY(id_guide) REFERENCES personnel(id_pers) ON DELETE CASCADE
);

CREATE TABLE gardien(
   id_gard INT,
   prime VARCHAR(50),
   CONSTRAINT PK_GARDIEN PRIMARY KEY(id_gard),
   CONSTRAINT FK_GARDIEN_PERSONNEL FOREIGN KEY(id_gard) REFERENCES personnel(id_pers) ON DELETE CASCADE
);

CREATE TABLE artiste(
   id_artiste INT AUTO_INCREMENT,
   nom_artiste VARCHAR(50),
   prenom_artiste VARCHAR(50),
   date_nais_art DATE,
   date_mort_art DATE,
   CONSTRAINT PK_ARTISTE PRIMARY KEY(id_artiste)
);

CREATE TABLE exposition(
   id_expo INT AUTO_INCREMENT,
   nom_expo VARCHAR(50),
   date_deb DATE NOT NULL,
   date_fin DATE,
   CONSTRAINT PK_EXPOSITION PRIMARY KEY(id_expo)
);

CREATE TABLE salle(
   num_salle INT,
   etage INT,
   id_expo INT,
   CONSTRAINT PK_SALLE PRIMARY KEY(num_salle, etage),
   CONSTRAINT FK_SALLE_EXPOSITION FOREIGN KEY(id_expo) REFERENCES exposition(id_expo) ON DELETE CASCADE
);

CREATE TABLE tarif(
   nom_tarif ENUM("Plein", "Reduit", "Enfant", "Pass_Culture", "Gratuit"),
   specification VARCHAR(50),
   prix LONG,
   CONSTRAINT PK_TARIF PRIMARY KEY(nom_tarif)
);

CREATE TABLE visiteur(
   id_visiteur INT AUTO_INCREMENT,
   nom_visiteur VARCHAR(50),
   prenom_visiteur VARCHAR(50),
   categorie VARCHAR(50),
   langue_visiteur VARCHAR(50),
   mail VARCHAR(50),
   newsletter BOOLEAN,
   id_guide INT,
   CONSTRAINT PK_VISITEUR PRIMARY KEY(id_visiteur),
   CONSTRAINT FK_VISITEUR_GUIDE FOREIGN KEY(id_guide) REFERENCES guide(id_guide) ON DELETE CASCADE
);

CREATE VIEW abonne
AS SELECT * FROM visiteur WHERE mail IS NOT NULL AND newsletter IS NOT NULL;

CREATE TABLE oeuvre(
   id_oeuvre INT AUTO_INCREMENT,
   nom_oeuvre VARCHAR(50),
   date_creation DATE,
   type_ VARCHAR(50),
   courant VARCHAR(50),
   id_expo INT NOT NULL,
   CONSTRAINT PK_OEUVRE PRIMARY KEY(id_oeuvre),
   CONSTRAINT FK_OEUVRE_EXPOSITION FOREIGN KEY(id_expo) REFERENCES exposition(id_expo) ON DELETE CASCADE
);

CREATE TABLE creer(
   id_artiste INT,
   id_oeuvre INT,
   CONSTRAINT PK_CREER PRIMARY KEY(id_artiste, id_oeuvre),
   CONSTRAINT FK_CREER_ARTISTE FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste) ON DELETE CASCADE,
   CONSTRAINT FK_CREER_OEUVRE FOREIGN KEY(id_oeuvre) REFERENCES oeuvre(id_oeuvre) ON DELETE CASCADE
);

CREATE TABLE surveille(
   id_gard INT,
   num_salle INT,
   etage INT,
   CONSTRAINT PK_SURVEILLE PRIMARY KEY(id_gard, num_salle, etage),
   CONSTRAINT FK_SURVEILLE_GARDIEN FOREIGN KEY(id_gard) REFERENCES gardien(id_gard) ON DELETE CASCADE,
   CONSTRAINT FK_SURVEILLE_SALLE FOREIGN KEY(num_salle, etage) REFERENCES salle(num_salle, etage) ON DELETE CASCADE
);

CREATE TABLE visite(
   nom_tarif ENUM("Plein", "Reduit", "Enfant", "Pass_Culture", "Gratuit") DEFAULT "Plein",
   id_visiteur INT NOT NULL,
   id_expo INT NOT NULL,
   date_ DATE NOT NULL,
   CONSTRAINT PK_VISITE PRIMARY KEY(nom_tarif, id_visiteur, id_expo, date_),
   CONSTRAINT FK_VISITE_TARIF FOREIGN KEY(nom_tarif) REFERENCES tarif(nom_tarif) ON DELETE CASCADE,
   CONSTRAINT FK_VISITE_VISITEUR FOREIGN KEY(id_visiteur) REFERENCES visiteur(id_visiteur) ON DELETE CASCADE,
   CONSTRAINT FK_VISITE_EXPOSITION FOREIGN KEY(id_expo) REFERENCES exposition(id_expo) ON DELETE CASCADE
);

-- Remplissage des tables
INSERT INTO personnel (nom_pers, prenom_pers, date_nais_pers, adresse_pers, salaire) VALUES
("Claribel", "Therese", '1997-06-10', "85 Rue de Strasbourg", 7064),
("Bernardo", "Eugène", '1992-05-23', "72 Rue de Strasbourg", 2898),
("Philadelphia", "Poornima", '1963-08-19', "89 rue des Coudriers", 6575),
("Reynaud", "Maddy", '1988-01-30', "86 Rue de Strasbourg", 5107),
("King", "Teodoro", '1990-10-03', "85 rue des Coudriers", 2811),
("Cristian", "Samra", '1995-07-10', "44 Avenue des Pres", 2608),
("Harun", "Bram", '1971-02-26', "20 Rue Frédéric Chopin", 630),
("Begonia", "Arun", '1964-05-05', "61 rue Sadi Carnot", 6128),
("Valarie", "Delicia", '1974-06-22', "80 rue Adolphe Wurtz", 9389),
("Delphia", "Najma", '1991-12-23', "7 rue Sadi Carnot", 6445),
("Melba", " Sameera", '1982-05-30', "84 rue Grande Fusterie", 5941),
("Suzan", "Mauricette", '1991-12-06', "45 rue Petite Fusterie", 3729),
("Bernardo", "Argyro", '2000-02-02', "24 Avenue De Marlioz", 8483),
("Kyro", "Maximiano", '1993-09-23', "33 Chemin Des Bateliers", 4924),
("Hortense", "Fidela", '1993-09-23', "31 rue Marguerite", 7574),
("Adela", "Briana", '1984-06-12', "25 rue du Fosse des Tanneurs", 6153),
("Saeed", "Mauricette", '1994-07-16', "23 Rue St Ferreol", 7828),
("Aliyya", "Maximillian", '1986-04-15', "26 Place du Jeu de Paume", 6241),
("Dell", "Susie", '1998-07-18', "67 Rue de Verdun", 6862),
("Cesario", "Marianne", '1989-02-08', "20 avenue de Provence", 7519),
("Spencer", "Patricia", '1997-05-12', "64 rue Pierre Motte", 5470),
("Sky", " Marina", '1985-01-01', "79 boulevard Amiral Courbet", 6419),
("Denis", "Esther", '2001-12-25', "42 Rue St Ferreol", 1186),
("Dell", "Patricia", '1994-11-03', "47 boulevard d'Alsace", 5611),
("Yasir", "Daniel", '1982-12-13', "3 Boulevard de Normandie", 4094),
("Cairo", "Mat", '1969-10-12', "11 Avenue Millies Lacroix", 8534),
("Ashley", "Tylor", '1973-03-11', "85 Chemin Des Bateliers", 1737),
("Finnegan", "Raymond", '1984-04-26', "32 rue Banaudon", 4542),
("Hortense", " Bonifacio", '1975-11-25', "88 avenue Ferdinand de Lesseps", 9745),
("Saeed", "Norbert", '1971-02-26', "25 rue des Nations Unies", 2486);

INSERT INTO gardien VALUES
(1, 500),
(2, 500),
(3, 500),
(4, 500),
(5, 500),
(6, 500),
(7, 500),
(8, 500),
(9, 500),
(10, 500);

INSERT INTO guide VALUES
(11, "Espagnol"),
(12, "Anglais"),
(15, "Grec"),
(17, "Italien"),
(18, "Allemand"),
(20, "Anglais");

INSERT INTO guide (id_guide) VALUES
(13),
(14),
(16),
(19);

INSERT INTO exposition (nom_expo, date_deb, date_fin) VALUES 
('Surrealisme', '2018-05-01', '2018-06-01'),
('Art Academique', '2018-07-01', '2018-09-01'),
('Impressionnisme', '2018-10-01', '2018-12-01'),
('Minimalisme', '2019-01-01', '2019-03-01'),
('Expressionnisme', '2019-04-01', '2019-05-01'),
('Pop Art', '2019-06-01', '2019-10-01'),
('Haute Renaissance', '2019-11-01', '2020-02-01'),
('Art Morderne', '2020-03-01', '2020-05-01'),
('Art Naif', '2020-06-01', '2020-10-01'),
('Permanente', '2018-05-01', NULL);

INSERT INTO salle VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 3),
(1, 2, 4),
(2, 2, 5),
(3, 2, 6),
(4, 2, 7),
(1, 3, 8),
(2, 3, 9),
(3, 3, 10),
(4, 3, 10);

INSERT INTO surveille VALUES
(1, 1, 1),
(3, 2, 1),
(1, 3, 1),
(4, 4, 1),
(2, 1, 2),
(2, 2, 2),
(4, 3, 2),
(5, 4, 2),
(6, 1, 3),
(7, 1, 3),
(8, 2, 3),
(9, 3, 3),
(10, 4, 3);

INSERT INTO visiteur (nom_visiteur, prenom_visiteur, categorie, langue_visiteur, mail, newsletter, id_guide) VALUES
("Henderson", "Abraham", "Professionnel", "Francais", "a.henderson@randatmail.com", 1, 13),
("Hall", "Alfred", null, "Anglais", null, 0, 12),
("Ross", "Alina", "Membre", null, "a.ross@randatmail.com", 1, null),
("Hawkins", "Kelsey", "18-25 ans", "Allemand", "k.hawkins@randatmail.com", 1, 18),
("Richards", "Amanda", "Etudiant", "Italien", null, 0, 17),
("Hall", "Blake", "-18 ans", null, "b.hall@randatmail.com", 1, null),
("Cole", "Adam", null, "Francais", "a.cole@randatmail.com", 1, 14),
("Mason", "Steven", "Membre", "Anglais", "s.mason@randatmail.com", 1, 20),
("Johnston", "Vincent", null, "Grec", "v.johnston@randatmail.com", 1, 15),
("Henderson", "Lilianna", "Membre", null, null, 0, null);

INSERT INTO tarif VALUES
("Plein", null, 18), -- null
("Reduit", "Présenter la carte etudiante", 13), -- 18-25 ans / Etudiant
("Enfant", "Moins de 18 ans", 8), -- -18 ans
("Pass_Culture", "Présenter la carte pass culture", 5), -- Professionnel
("Gratuit", "Présenter la carte prestige", 0); -- Membre

INSERT INTO artiste (nom_artiste, prenom_artiste, date_nais_art, date_mort_art) VALUES 
('Dali', 'Salvador', '1904-05-11', '1989-01-23'),
('Cabanel', 'Alexandre', '1823-09-28', '1889-01-23'),
('Renoir', 'Auguste', '1841-02-25', '1919-12-03'),
('Soulages', 'Pierre', '1919-12-24', NULL),
('Bacon', 'Francis', '1909-10-28', '1992-04-28'),
('Warhol', 'Andy', '1928-08-06', '1987-02-22'),
('De Vinci', 'Leonard', '1452-04-14', '1519-05-02'),
('Rodin', 'Auguste', '1840-11-12', '1917-11-17'),
('Kahlo', 'Frida', '1907-07-06', '1954-07-13'),
('Haring', 'Keith', '1958-05-04', '1990-02-16');

INSERT INTO oeuvre (nom_oeuvre, date_creation, type_, courant, id_expo) VALUES
('La Persistance de la Memoire', '1931-01-01', 'Peinture a l huile, bronze', 'Surrealisme', 1),
('Metamorphose de Narcisse', '1937-01-01', 'Peinture a l huile', 'Surrealisme', 1),
('L ange Dechu', '1847-01-01', 'Peinture a l huile', 'Art Academique', 2),
('Phedre', '1880-01-01', 'Peinture a l huile', 'Art Academique', 2),
('Le Dejeuner des Canotiers', '1880-01-01', 'Peinture a l huile', 'Impressionnisme, Art Moderne', 3),
('Les Grandes Baigneuses', '1884-01-01', 'Peinture a l huile', 'Impressionnisme, Art Moderne', 3),
('Composition', '1951-01-01', 'Peinture', 'Minimalisme', 4),
('Gouache 2007 B-7', '2004-01-01', 'Peinture', 'Minimalisme, Art Abstrait', 4),
('Trois Etudes de Lucian Freud', '1969-01-01', 'Peinture a l huile', 'Expressionnisme', 5),
('Two Figures', '1953-01-01', 'Peinture a l huile', 'Expressionnisme', 5),
('Campbell s Soup Cans', '1962-01-01', 'Peinture Acrylique', 'Pop Art', 6),
('Camouflage Self-Portrait', '1986-01-01', 'Peinture a l huile', 'Pop Art', 6),
('La Joconde', '1503-01-01', 'Peinture a l huile', 'Renaissance', 7),
('Montgolfiere', '1500-01-01', 'Invention', 'Renaissance', 7),
('Le Penseur', '1880-01-01', 'Sculpture', 'Art Moderne, Impressionisme', 8),
('La Danaide', '1885-01-01', 'Sculpture', 'Art Moderne, Impressionisme', 8),
('Les Deux Fridas', '1939-01-01', 'Peinture', 'Art Naif, Surrealisme', 9),
('La Colonne Brisee', '1944-01-01', 'Peinture', 'Art Naif, Surrealisme, Art Moderne', 9),
('Radiant Baby', '1990-01-01', 'Peinture a l huile', 'Pop Art, Art Abstrait', 10),
('Safe Sex', '1988-01-01', 'Peinture a l huile', 'Pop Art', 10),
('General Henry Knox', '1750-01-01', 'Peinture a l huile', 'Classicisme', 10);

INSERT INTO creer (id_artiste, id_oeuvre) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(6, 11),
(6, 12),
(7, 3),
(7, 14),
(8, 15),
(8, 16),
(9, 17),
(9, 18),
(10, 19),
(10, 20);


INSERT INTO visite (nom_tarif, id_visiteur, id_expo, date_) VALUES
("Pass_Culture", 1, 4, '2019-02-17'),
("Plein", 2, 1, '2018-05-13'),
("Reduit", 4, 2, '2018-07-26'),
("Gratuit", 3, 3, '2018-10-16'),
("Gratuit", 3, 3, '2018-11-29'),
("Plein", 9, 4, '2019-01-24'),
("Pass_Culture", 1, 5, '2019-04-02'),
("Pass_Culture", 1, 5, '2019-04-22'),
("Reduit", 5, 5, '2019-05-30'),
("Reduit", 5, 6, '2019-06-25'),
("Pass_Culture", 1, 6, '2019-09-13'),
("Pass_Culture", 1, 7, '2019-12-20'),
("Reduit", 4, 7, '2020-01-18'),
("Gratuit", 8, 8, '2020-03-05'),
("Plein", 7, 8, '2020-04-24'),
("Enfant", 6, 8, '2020-04-24'),
("Gratuit", 10, 9, '2020-08-02'),
("Reduit", 4, 10, '2021-11-17'),
("Pass_Culture", 1, 2, '2019-04-17'),
("Pass_Culture", 1, 3, '2019-05-17'),
("Pass_Culture", 1, 5, '2019-06-17'),
("Pass_Culture", 1, 6, '2019-07-17'),
("Pass_Culture", 1, 7, '2019-08-17'),
("Pass_Culture", 1, 8, '2019-09-17'),
("Pass_Culture", 1, 9, '2019-10-17'),
("Pass_Culture", 1, 1, '2019-11-17'),
("Pass_Culture", 1, 10, '2019-12-17');

-- Creation Trigger/Fonction/Procedure
DELIMITER $$
-- Trigger
-- Si l'on souhaite supprimer une salle il est possible
-- car toute ligne dans "surveille" avec cette salle en FK sera supprimee
-- équivalent du on delete CASCADE
CREATE TRIGGER avant_suppression_salle
BEFORE DELETE
ON salle FOR EACH ROW
BEGIN
    DELETE FROM surveille WHERE num_salle=OLD.num_salle AND etage=OLD.etage;
END$$
DELIMITER ;

DELIMITER $$
-- Trigger
-- Permet de vérifier si la catégorie de tarif du visiteur est valide
-- cad si elle fait partie de l'enssemble suivant:
-- {"-18 ans", "18-25 ans", "Professionnel", "Etudiant", "Membre", null}
CREATE TRIGGER categorie_incorrecte
BEFORE INSERT
ON visiteur FOR EACH ROW
BEGIN

    DECLARE categorie VARCHAR(50);
    DECLARE tarif_correspondant VARCHAR(50);
    IF NEW.categorie!="-18 ans" AND NEW.categorie!="18-25 ans" AND NEW.categorie != "Professionnel" AND NEW.categorie != "Etudiant" AND NEW.categorie != "Membre" AND NEW.categorie IS NOT NULL THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = "Erreur: Categorie non trouvee";
    END IF;
END;
$$
DELIMITER ;

DELIMITER $$

-- Trigger
-- Permet de vérifier si l'oeuvre a bien été produite durant la durée de vie de l'artiste
-- cad si sa date de création est bien comprise entre la date de naissance et de mort de l'artiste


CREATE TRIGGER artiste_mort
BEFORE INSERT of id_artiste, id_oeuvre
ON creer FOR EACH ROW
DECLARE
   date_oeuvre DATE;
   date_nais_artiste DATE;
   date_mort_artiste DATE;
BEGIN
   SELECT date_creation INTO date_oeuvre from oeuvre where id_oeuvre= :new.id_oeuvre;
   SELECT date_nais_art INTO date_nais_art from artiste where id_artiste= :new.id_artiste;
   SELECT date_mort_art INTO date_mort_art from artiste where id_artiste= :new.id_artiste;
   IF date_creation NOT BETWEEN date_nais_art and date_mort_art THEN
      RAISE_APPLICATION_ERROR(-20505,'Impossible, Artiste mort');
   END IF;
END;

$$
DELIMITER ;
-- Fonction
-- Retourne le tarif correpondant a la categorie du visiteur
CREATE FUNCTION get_tarif_from_categorie(categorie VARCHAR(50))
RETURNS VARCHAR(50)
BEGIN
    DECLARE tarif_correspondant VARCHAR(50);
    IF categorie = "Professionnel" THEN
        SELECT nom_tarif INTO tarif_correspondant FROM tarif WHERE nom_tarif="Pass_Culture";
    ELSEIF categorie = "-18 ans" THEN
        SELECT nom_tarif INTO tarif_correspondant FROM tarif WHERE nom_tarif="Enfant";
    ELSEIF categorie = "18-25 ans" OR categorie = "Etudiant" THEN
        SELECT nom_tarif INTO tarif_correspondant FROM tarif WHERE nom_tarif="Reduit";
    ELSEIF categorie = "Membre" THEN
        SELECT nom_tarif INTO tarif_correspondant FROM tarif WHERE nom_tarif="Gratuit";
    ELSEIF categorie = null THEN
        SELECT nom_tarif INTO tarif_correspondant FROM tarif WHERE nom_tarif="Plein"; 
    END IF;
    RETURN tarif_correspondant;
END$$
DELIMITER ;

DELIMITER $$
-- Procedude
-- Retorune le tarif correspondant au visiteur
-- en fonction de son identifiant
CREATE PROCEDURE get_tarif(IN id INT, OUT res VARCHAR(50))
BEGIN
    DECLARE categ VARCHAR(50);
    SELECT categorie INTO categ FROM visiteur WHERE id_visiteur=id;
    SET res = get_tarif_from_categorie(categ);
END$$
DELIMITER ;
