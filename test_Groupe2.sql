#CASSAN Jules 21905789
#TRIMBUR Léo 22210320

-- Récupérer tous les artistes ainsi que le nombre d'oeuvre de ces derniers
-- qui sont présent durant l'expo permanente
SELECT a.nom_artiste, COUNT(o.id_oeuvre) AS "nboeuvre"
FROM artiste a, creer c, oeuvre o, exposition e
where a.id_artiste=c.id_artiste AND c.id_oeuvre=o.id_oeuvre AND o.id_expo=e.id_expo
AND e.date_fin IS NULL
GROUP BY id_artiste,nom_artiste;


--Les visiteurs qui ont visité toutes les expositions
SELECT * from visiteur
	where not exists(
  		select * from exposition
  		where not exists
  			(
    			select * from visite
    			where visiteur.id_visiteur = visite.id_visiteur
    			and visite.id_expo = exposition.id_expo
  			)
	);


-- L'employé ayant le salaire le plus élevé
SELECT p.nom_pers, p.prenom_pers
from personnel p where salaire=(select MAX(pe.salaire)
                                from personnel pe);

-- Les artistes qui sont né pendant le XXème siècle

select a.id_artiste,a.nom_artiste,a.prenom_artiste
from artiste a
where a.id_artiste IN (select id_artiste from artiste 
	where date_nais_art between '1900-01-01' and '2000-01-01');

-- Cette requête a pour but de sélectionner les noms des expositions et leur nombre de visiteurs
-- Lorsque celles-ci ont eu plus de 3 visiteurs

select e.id_expo, e.nom_expo, count(*) as "Nombre de visiteurs"
from exposition e, visite v
where e.id_expo=v.id_expo
and e.id_expo in (select e.id_expo from exposition e, visite v where e.id_expo=v.id_expo group by e.id_expo having count(*)>=3)
group by e.id_expo, e.nom_expo;

-- Test des triggers

-- Ajout de visiteur avec catégorie invalide
insert into visiteur (nom_visiteur, prenom_visiteur, categorie, langue_visiteur, mail, id_guide) values ("Bekham", "David", "Sportif", "Anglais", null, null);
insert into visiteur (nom_visiteur, prenom_visiteur, categorie, langue_visiteur, mail, id_guide) values ("Cassan", "Jules", "Poète", "Français", null, null);
insert into visiteur (nom_visiteur, prenom_visiteur, categorie, langue_visiteur, mail, id_guide) values ("Trimbur", "Léo", "Sportif", "Anglais", null, null);

-- Ajout d'une oeuvre erronée
INSERT INTO ARTISTE(id_artiste,nom_artiste,prenom_artiste,date_nais_art,date_mort_art) 
VALUES(25,'Cassan','Jules','2001-07-25',null);

INSERT INTO oeuvre (id_oeuvre,nom_oeuvre, date_creation, type_, courant, id_expo) VALUES
(25,'Projet de BDD', '1931-01-01', 'SQL', 'Surrealisme', 1);

INSERT INTO creer (id_artiste, id_oeuvre) VALUES
(25, 25);

-- Test de la procédure
call get_tarif(4, @cat);
select @cat;
-- Test erroné de la procédure
call get_tarif(114, @cat);
select @cat;
